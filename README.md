## About
This macro for the Nice Editor ([http://ne.di.unimi.it](http://ne.di.unimi.it)) executes shell scripts and write the output directly in the same document of the editor.

To use it, just start the Nice Editor, then open the menu section ```Macros -> Open macro...```, so select ```nice-bash.macro```.

Write your shell script in your document, then a line ```### output below```, finally a newline after it.

You can execute the macro via menu ```Macros -> Play Once``` or pressing ```F9```: the output will be printed directly at the bottom of the document.

You can change your script and re-run the macro any time, the output will be updated.

## Examples
After loading the macro, open a new document and enter the following:
```
echo "Hello world"
pwd
uname -a

### output below


```
After running the macro, you should see the document updated with something similar to:
```
echo "Hello world"
pwd
uname -a

### output below


Hello world
/home/raffaele
Linux aldebaran 3.16.0-4-amd64 #1 SMP Debian 3.16.36-1+deb8u2 (2016-10-19) x86_64 GNU/Linux
```

## License
Public domain
